package eu.evacuate.og.ws.client.urlmanager;

public class UrlManager {


    private String szURLHost = "http://192.168.109.40:";
   
    private String szURLPort = "8080";
    private String szBaseURL="/OperatorGateway/api/";
    
    
    //Post
    private String urlControl=szURLHost+szURLPort+szBaseURL+"Control";
    private String urlTetra=szURLHost+szURLPort+szBaseURL+"TETRA_MSG";
    private String urlSTXPhone=szURLHost+szURLPort+szBaseURL+"STX_PHONE";
    private String urlDynamicExitSign=szURLHost+szURLPort+szBaseURL+"DES";
    private String urlDigitalSign=szURLHost+szURLPort+szBaseURL+"DigitalSign";

    /**
     * @return the szURLHost
     */
    public String getSzURLHost() {
        return szURLHost;
    }

    /**
     * @param szURLHost the szURLHost to set
     */
    public void setSzURLHost(String szURLHost) {
        this.szURLHost = szURLHost;
    }

    /**
     * @return the szURLPort
     */
    public String getSzURLPort() {
        return szURLPort;
    }

    /**
     * @param szURLPort the szURLPort to set
     */
    public void setSzURLPort(String szURLPort) {
        this.szURLPort = szURLPort;
    }

    /**
     * @return the szBaseURL
     */
    public String getSzBaseURL() {
        return szBaseURL;
    }

    /**
     * @param szBaseURL the szBaseURL to set
     */
    public void setSzBaseURL(String szBaseURL) {
        this.szBaseURL = szBaseURL;
    }

    /**
     * @return the urlControl
     */
    public String getUrlControl() {
        return urlControl;
    }

    /**
     * @param urlControl the urlControl to set
     */
    public void setUrlControl(String urlControl) {
        this.urlControl = urlControl;
    }

    /**
     * @return the urlTetra
     */
    public String getUrlTetra() {
        return urlTetra;
    }

    /**
     * @param urlTetra the urlTetra to set
     */
    public void setUrlTetra(String urlTetra) {
        this.urlTetra = urlTetra;
    }

    /**
     * @return the urlSTXPhone
     */
    public String getUrlSTXPhone() {
        return urlSTXPhone;
    }

    /**
     * @param urlSTXPhone the urlSTXPhone to set
     */
    public void setUrlSTXPhone(String urlSTXPhone) {
        this.urlSTXPhone = urlSTXPhone;
    }

    /**
     * @return the urlDynamicExitSign
     */
    public String getUrlDynamicExitSign() {
        return urlDynamicExitSign;
    }

    /**
     * @param urlDynamicExitSign the urlDynamicExitSign to set
     */
    public void setUrlDynamicExitSign(String urlDynamicExitSign) {
        this.urlDynamicExitSign = urlDynamicExitSign;
    }

    /**
     * @return the urlDigitalSign
     */
    public String getUrlDigitalSign() {
        return urlDigitalSign;
    }

    /**
     * @param urlDigitalSign the urlDigitalSign to set
     */
    public void setUrlDigitalSign(String urlDigitalSign) {
        this.urlDigitalSign = urlDigitalSign;
    }
    
    
    
    
    
}
