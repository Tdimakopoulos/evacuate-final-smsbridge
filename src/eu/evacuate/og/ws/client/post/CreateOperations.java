package eu.evacuate.og.ws.client.post;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import eu.evacuate.og.ws.client.urlmanager.UrlManager;
import eu.evacuate.og.ws.dto.ControlDTO;
import eu.evacuate.og.ws.dto.DigitalSignDTO;
import eu.evacuate.og.ws.dto.DynamicExitSignDTO;
import eu.evacuate.og.ws.dto.ResponseDTO;
import eu.evacuate.og.ws.dto.STXPhoneDTO;
import eu.evacuate.og.ws.dto.TetraDTO;
import javax.ws.rs.core.MultivaluedMap;

public class CreateOperations {

    ObjectMapper mapper = new ObjectMapper();
    UrlManager URL = new UrlManager();
    String URLSMS = "http://192.168.108.250:6682/api/network/captive/sms";
    String GetAssociatedPeople="http://192.168.108.250:6682/api/network/accesspoint/9ac13b44a3b23f6394d5064558f1cf67/associated";
    

//http://192.168.108.130:6682/api/network/accesspoint/9ac13b44a3b23f6394d5064558f1cf67/associated
//http://192.168.108.130:6682/api/network/accesspoint/dd3ea593f5f4381495c60503787d41f9/associated
//http://192.168.108.130:6682/api/network/accesspoint/55603da97ec23c0987c5906d52102023/associated
//http://192.168.108.130:6682/api/network/accesspoint/7e1ef4103a5e3c099745e2660125497b/associated
    
//http://192.168.108.130:6682/api/network/accesspoint/9ac13b44a3b23f6394d5064558f1cf67/heard
//http://192.168.108.130:6682/api/network/accesspoint/dd3ea593f5f4381495c60503787d41f9/heard
//http://192.168.108.130:6682/api/network/accesspoint/55603da97ec23c0987c5906d52102023/heard
//http://192.168.108.130:6682/api/network/accesspoint/7e1ef4103a5e3c099745e2660125497b/heard
    
    public String SendSMMToAll(String SMS) {

        String smsJson = "{    \"filter\": \"none\",    \"content\": \"" + SMS + "\"}";
        Client client = Client.create();

        WebResource webResource = client.resource(URLSMS);

        try {
            webResource.type("application/json")
                    .put(String.class, smsJson);
        } catch (Exception ex) {
            System.out.println("Error : " + ex.getMessage());
        }
        return "ÖK";

    }

    
    public String SendSMMToInRange(String SMS) {

        String smsJson = "{    \"filter\": \"associated\",    \"content\": \"" + SMS + "\"}";
        Client client = Client.create();
        WebResource webResource = client.resource(GetAssociatedPeople);

        
try
{
        webResource.type("application/json")
                .put(String.class, smsJson);
        } catch (Exception ex) {
            System.out.println("Error : " + ex.getMessage());
        }
        return "OK";

    }

    public boolean evacControlSystem(
            ControlDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlControl());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return true;
    }

    public ResponseDTO evacTetra(
            TetraDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlTetra());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return mapper.readValue(szReturn, ResponseDTO.class);
    }

    public ResponseDTO evacDigitalSign(
            DigitalSignDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);
        System.out.println("Json to OG : " + ControlJSON);
        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlDigitalSign());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return mapper.readValue(szReturn, ResponseDTO.class);
    }

    public ResponseDTO evacDigitalExitSign(
            DynamicExitSignDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlDynamicExitSign());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return mapper.readValue(szReturn, ResponseDTO.class);
    }

    public ResponseDTO evacSTXPhone(
            STXPhoneDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlSTXPhone());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return mapper.readValue(szReturn, ResponseDTO.class);
    }

}
