/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.evacuate.og.ws.dto;

/**
 *
 * @author tdim
 */
public class STXPhoneDTO {
    private String procedure;
    private String message;

    /**
     * @return the procedure
     */
    public String getProcedure() {
        return procedure;
    }

    /**
     * @param procedure the procedure to set
     */
    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
