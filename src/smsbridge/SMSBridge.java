/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smsbridge;

import com.indra.sofia2.ssap.kp.implementations.rest.exception.ResponseMapperException;
import eu.evacuate.og.ws.client.post.CreateOperations;
import java.io.IOException;
import org.exus.alarm.sms.publishalarm;
import org.primefaces.json.JSONException;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class SMSBridge {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ResponseMapperException, IOException, JSONException {
        System.out.println("Starting Evacuate Start/End Server !");
        publishalarm pa = new publishalarm();
//        CreateOperations popx = new CreateOperations();
//                    popx.SendSMMToAll("Evacuation Start");
        pa.PublishAlarm();
        int istart = pa.QueryOnto();

        int istart2 = pa.QueryOnto2();
        while (true) {
            try {
                Thread.sleep(30000);
                System.out.println("Check");
                int inew = pa.QueryOnto();
                int inew2 = pa.QueryOnto2();

                if (istart == inew) {
                    System.out.println("Nothing");
                } else {
                    istart=inew;
                    System.out.println("Evacuate Start!!");
                    CreateOperations pop = new CreateOperations();
                    pop.SendSMMToAll("Evacuation Start");
                }

                if (istart2 == inew2) {
                    System.out.println("Nothing");
                } else {
                    istart2=inew2;
                    System.out.println("Evacuate Ends!!");
                    CreateOperations pop = new CreateOperations();
                    pop.SendSMMToAll("Evacuation Ends");
                }

            } catch (InterruptedException ex) {
                System.out.println("error on wait");
            }

        }
    }
    
}
