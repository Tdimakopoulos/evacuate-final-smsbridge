/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.exus.alarm.sms;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author Thomas Dimakopoulos
 */
public final class Exitsignstatus {
    public final _id _id;
    public final AgentExitSign agentExitSign;
    public final ContextData contextData;

    @JsonCreator
    public Exitsignstatus(@JsonProperty("_id") _id _id, @JsonProperty("agentExitSign") AgentExitSign agentExitSign, @JsonProperty("contextData") ContextData contextData){
        this._id = _id;
        this.agentExitSign = agentExitSign;
        this.contextData = contextData;
    }

    public static final class _id {
        public final String $oid;

        @JsonCreator
        public _id(@JsonProperty("$oid") String $oid){
            this.$oid = $oid;
        }
    }

    public static final class AgentExitSign {
        public final String identifier;
        public final String timestamp;
        public final String commstatus;

        @JsonCreator
        public AgentExitSign(@JsonProperty("identifier") String identifier, @JsonProperty("timestamp") String timestamp, @JsonProperty("commstatus") String commstatus){
            this.identifier = identifier;
            this.timestamp = timestamp;
            this.commstatus = commstatus;
        }
    }

    public static final class ContextData {
        public final String kp;
        public final String kp_instancia;
        public final String session_key;
        public final Timestamp timestamp;
        public final String user;

        @JsonCreator
        public ContextData(@JsonProperty("kp") String kp, @JsonProperty("kp_instancia") String kp_instancia, @JsonProperty("session_key") String session_key, @JsonProperty("timestamp") Timestamp timestamp, @JsonProperty("user") String user){
            this.kp = kp;
            this.kp_instancia = kp_instancia;
            this.session_key = session_key;
            this.timestamp = timestamp;
            this.user = user;
        }

        public static final class Timestamp {
            public final String $date;
    
            @JsonCreator
            public Timestamp(@JsonProperty("$date") String $date){
                this.$date = $date;
            }
        }
    }
}